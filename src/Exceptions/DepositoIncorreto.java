package Exceptions;

public class DepositoIncorreto extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public DepositoIncorreto(String msn) {
		super(msn);
	}
}

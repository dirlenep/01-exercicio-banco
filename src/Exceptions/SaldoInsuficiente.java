package Exceptions;

public class SaldoInsuficiente extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public SaldoInsuficiente(String msn) {
		super(msn);
	}
}


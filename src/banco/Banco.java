package banco;

public class Banco {
	
	private String nome;
	private String cnpj;
	private int agencia;
	private Cliente[] clientes;
	
	public Banco(String nome, String cnpj, int agencia) {
		this.nome = nome;
		this.cnpj = cnpj;
		setAgencia(agencia);
		this.clientes = new Cliente[0];
	}
	
	public int getAgencia() {
		return agencia;
	}
	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}
	public void setCliente(Cliente[] clientes) {
		this.clientes = clientes;
	}
	public Cliente[] getClientes() {
		return this.clientes;
	}
	
	public Cliente getCliente(String cpf) {
		for (int i = 0; i < getClientes().length; i++) {
			if (this.clientes[i].getCpf() == cpf) {
				return this.clientes[i];
			}
		}
		return this.clientes[0];
	}
	
	public void adicionarCliente(Cliente cliente) {
		Cliente[] clientesTemp = new Cliente[this.clientes.length + 1];
		for (int i = 0; i < getClientes().length; i++) {
			clientesTemp[i] = getClientes()[i];
		}
		clientesTemp[clientesTemp.length - 1] = cliente;
		setCliente(clientesTemp);
	}
}

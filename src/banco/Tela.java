package banco;

import java.util.Scanner;

import Exceptions.DepositoIncorreto;
import Exceptions.SaldoInsuficiente;

public class Tela {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Conta novaConta = new Conta(001, 100.00, 200.00, 1, 1234);
		Cliente novoCliente = new Cliente("Janete", "123.456.789-10", "0111222", "Rua XV de Novembro, 100", novaConta);
		Banco novoBanco = new Banco("Bradesco", "01.001.001/0001-01", 10);
		novoBanco.adicionarCliente(novoCliente);
				
		System.out.println(novoCliente);
		System.out.println(novoCliente.getConta());
		System.out.println(novaConta.getSaldo()); 
		
		Scanner teclado = new Scanner(System.in);
		byte opcao = 5;

		while (opcao != 0) {
			System.out.println("Digite 1 para Depositar");
			System.out.println("Digite 2 para Sacar");
			System.out.println("Digite 0 para Sair");
			opcao = teclado.nextByte();
			
			if (opcao == 1) {
				try {
					novaConta.depositar(100);
					System.out.println(novaConta.getSaldo());
				} catch (DepositoIncorreto e) {
					System.out.println(e.getMessage());
				}
			} else if (opcao == 2) {
				try {
					novaConta.sacar(500);
					System.out.println(novaConta.getSaldo());
				} catch (SaldoInsuficiente e) {
					System.out.println(e.getMessage());
				}
			} else if (opcao == 0) {
				System.out.println("Saindo da aplica��o");
			}
		}
	}
}
